package controller;

import com.controler.ApiControler;
import com.model.Budget;
import com.service.BudgetService;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static io.restassured.RestAssured.given;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(MockitoJUnitRunner.class)
public class ApiControllerTest {

    @Mock
    private BudgetService budgetService;

    @InjectMocks
    private ApiControler controler;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        controler = new ApiControler();
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controler).build();
    }

    @Test
    @Ignore
    public void getRestApiBudgetTest() {
        given().
        when()
                .get("http://localhost:8080/budget")
        .then()
                .assertThat()
                .statusCode(200)
        .and()
                .contentType(ContentType.JSON);
    }

    @Test
    public void getApiBudgetTest() throws Exception {
        final Budget budget = new Budget(2000);
        Mockito.when(budgetService.getBudget()).thenReturn(asList(budget));
        final MvcResult mvcResult = mockMvc.perform(get("/budget"))
                .andExpect(status().isOk())
                .andReturn();
        assertTrue(mvcResult.getResponse().getStatus() == 200);
    }

    @Test
    public void putApiBudgetTest() throws Exception {
        final Budget budget = new Budget(2000);
        Mockito.when(budgetService.setBudget(anyInt())).thenReturn(budget);
        final MvcResult mvcResult = mockMvc.perform(get("/budget/{number}", 2000))
                .andExpect(status().isOk())
                .andReturn();
        assertTrue(mvcResult.getResponse().getStatus() == 200);
    }
}