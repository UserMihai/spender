package service;

import com.dao.BudgetDao;
import com.model.Budget;
import com.service.impl.BudgetServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BudgetServiceTest {

    @Mock
    private BudgetDao budgetDao;

    @InjectMocks
    private BudgetServiceImpl budgetService;

    @Test
    public void testGetBudget() {
        final List<Budget> budgetList = asList(new Budget(2000));
        when(budgetDao.getBudget()).thenReturn(budgetList);

        final List<Budget> list = budgetService.getBudget();
        assertEquals(budgetList, list);
    }

    @Test
    public void testSetBudget() {
        final Budget budget = new Budget(2000);
        when(budgetDao.setBudget(anyInt())).thenReturn(budget);

        final Budget budg = budgetService.setBudget(2000);
        assertEquals(budget, budg);
    }
}