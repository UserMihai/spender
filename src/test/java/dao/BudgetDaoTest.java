package dao;

import com.dao.impl.BudgetDaoImpl;
import com.model.Budget;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.eq;

@RunWith(MockitoJUnitRunner.class)
public class BudgetDaoTest {

    @Mock
    private EntityManager entityManager;

    @Mock
    private Query query;

    @InjectMocks
    private BudgetDaoImpl budgetDao;

    @Before
    public void setUp() {
        budgetDao = new BudgetDaoImpl(entityManager);
    }

    @Test
    public void testGetBudget() {
        final List<Budget> budgetList = asList(new Budget(2000));

        when(entityManager.createNativeQuery(anyString(), eq(Budget.class))).thenReturn(query);
        when(query.getResultList()).thenReturn(budgetList);

        final List<Budget> list = budgetDao.getBudget();

        assertEquals(budgetList, list);
    }

    @Test
    public void testPersistBudget() {
        final Budget budget = budgetDao.setBudget(2000);

        assertEquals(2000, budget.getNumber());
    }
}
