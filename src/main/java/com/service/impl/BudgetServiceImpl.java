package com.service.impl;

import com.dao.BudgetDao;
import com.model.Budget;
import com.service.BudgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BudgetServiceImpl implements BudgetService {

    @Autowired
    private BudgetDao budgetDao;

    public BudgetServiceImpl() {

    }

    @Override
    public Budget setBudget(final int number) {
        return budgetDao.setBudget(number);
    }

    @Override
    public List<Budget> getBudget() {
        return budgetDao.getBudget();
    }

}