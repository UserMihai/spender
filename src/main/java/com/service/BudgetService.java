package com.service;

import com.model.Budget;

import java.util.List;

public interface BudgetService {

    /**
     * Set the budget
     *
     * @param number
     * @return
     */
    Budget setBudget(int number);

    /**
     * Returns a list of budgets
     *
     * @return
     */
    List<Budget> getBudget();

}
