package com.controler;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;


@Controller
@RequestMapping(value = "/home")
public class ViewController {

    @RequestMapping(method = GET)
    public String home() {
        return "index.html";
    }
}
