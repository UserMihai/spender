package com.controler;

import com.model.Budget;
import com.service.BudgetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class ApiControler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiControler.class);

    @Autowired
    private BudgetService budgetService;

    @RequestMapping(value = "/budget/{number}", method = GET)
    public Budget setBudget(@PathVariable final int number) {
        LOGGER.info("Insert a new budget");
        return budgetService.setBudget(number);
    }

    @RequestMapping(value = "/budget", method = GET)
    public List<Budget> getBudget() {
        return budgetService.getBudget();
    }
}