package com.dao.impl;

import com.dao.BudgetDao;
import com.model.Budget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class BudgetDaoImpl implements BudgetDao{

    private final EntityManager entityManager;

    @Autowired
    public BudgetDaoImpl(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public List<Budget> getBudget() {
        final Query query = entityManager.createNativeQuery("select * from budget", Budget.class);
        return query.getResultList();
    }

    @Override
    public Budget setBudget(final int number) {
        Budget budget = new Budget(number);
        entityManager.persist(budget);
        return budget;
    }
}