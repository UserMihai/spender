package com.dao;

import com.model.Budget;

import java.util.List;

public interface BudgetDao {

    /**
     * Set the budget
     *
     * @param number
     * @return
     */
    Budget setBudget(int number);

    /**
     * Returns a list of budgets
     *
     * @return
     */
    List<Budget> getBudget();

}